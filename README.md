# Differential equations
## Computational Practicum

by Muhammad Mavlyutov, B17-08

m.mavlyutov@innopolis.ru

[@theMavl](http://t.me/theMavl)


## Report
### 1. Solution of IVP (Variant 13)
![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24%5Cbegin%7Bcases%7D%20y%27%3D%5Csin%5E2%20x%20&plus;%20y%20%5Ccot%20x%20%5C%5C%20y%281%29%20%3D%201%20%5C%5C%20x%20%5Cin%20%5B1%3B%203%5D%20%5Cend%7Bcases%7D%24)

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24y%27-y%20%5Ccot%20x%3D%5Csin%5E2%20x%24)

Let ![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24y_c%24) be a solution for complementary equation ![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24y%27-y%20%5Ccot%20x%3D0%24):

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%7Bdy%5Cover%20dx%7D%20%3D%20y%5Ccot%20x)

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%7Bdy%5Cover%20y%7D%3D%5Ccot%20x%20dx) (assuming ![](https://latex.codecogs.com/png.latex?%5Cbg_white%20y%20%5Cneq%200))

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24%5Cln%20y_c%3D%5Cln%20%5Csin%20x%20&plus;%20C%24)

Let C be 0:

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24y_c%3D%5Csin%20x%24)


Making substitution ![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24y%3Duy_c%24) to the original equation:

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24u%27y_c&plus;uy%27_c-uy_c%5Ccot%20x%3D%5Csin%5E2%20x%24)

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24u%27y_c&plus;u%28y%27_c-y_c%5Ccot%20x%29%3D%5Csin%5E2%20x%24)

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24u%27y_c%3D%5Csin%5E2%20x%24)

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%7Bdu%5Cover%20dx%7D%5Csin%20x%20%3D%20%5Csin%5E2x)

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24du%3D%5Csin%20x%20dx%24)

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24u%3D-%5Ccos%20x&plus;C%3DC-%5Ccos%20x%24)

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24%5Cboxed%7By%3DC%5Csin%20x-%5Csin%20x%20%5Ccos%20x%7D%24) - General solution.


Solving IVP:

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24y_0%3DC%5Csin%20x_0-%5Csin%20x_0%20%5Ccos%20x_0%24)

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%24y_0&plus;%5Csin%20x_0%20%5Ccos%20x_0%3DC%5Csin%20x_0%24)

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20C%20%3D%20%7By_0&plus;%5Csin%20x_0%20%5Ccos%20x_0%20%5Cover%20%5Csin%20x_0%7D)

![](https://latex.codecogs.com/png.latex?%5Cbg_white%20%7By%3D%7By_0&plus;%5Csin%20x_0%20%5Ccos%20x_0%20%5Cover%20%5Csin%20x_0%7D%5Csin%20x-%5Csin%20x%20%5Ccos%20x%7D%3D%5Cboxed%7B%5Csin%20x*%28%7By_0%5Cover%20%5Csin%20x_0%7D%20&plus;%20%5Ccos%20x_0%20-%20%5Ccos%20x%29%7D) - IVP solution.


### 2. Source code
[main.py](https://gitlab.com/theMavl/inno2018differentialequations_computationalpracticum/blob/master/main.py)

### 3. Launching
0. You must have Python installed on your machine. Make sure that you can run `py` (for Windows) or `python` (for Linux, but sometimes Windows also) command in your Command Line. 
1. Download repository
#### Windows
2. Navigate to `inno2018differentialequations_computationalpracticum-master/windows` 
3. If on step 0 command `py` didn't work, but you could run command `python`, you need to do this:
    1. Open Command Line in folder `inno2018differentialequations_computationalpracticum-master/windows` (click **RIGHT MOUSE BUTTON** somewhere inside the folder area while holding **SHIFT** and select **Open command promt**)
    2. In cmdline type `python -m venv venv`
    3. Once you see `venv` folder appeared, you can close the cmdline.
4.   Run `1. First run.bat`. Script will automatically create virtual environment in this folder and download all required packages into it.
5. Wait until installation is done. Script will also launch the program automatically, once it filishes preparing the environment.
7. To run program again, you can use `2. Simple run.bat` script

#### Linux
2. Navigate to `inno2018differentialequations_computationalpracticum-master/linux` 
3. Run `FirstRun.sh`. Script will automatically create virtual environment in this folder and download all required packages into it.
4. Wait until installation is done. Script will also launch the program automatically, once it filishes preparing the environment.
5. To run program again, you can use `SimpleRun.sh` script

If you did everything correctly, you will see the following lines in the terminal:
```
Input the initial conditions x0, X, y0, N
(x0 - start x point, X - end x point, y0 - initial value, N - grid blocks per unit)
Default input: 1 3 1 10
(press ENTER to use defaults)
```

Here you can just hit ENTER to apply the default initial conditions, or manually override them. An input is a single line of format:
`x0:float X:float y0:float, N:int`

For example, line
`1 3 1 10`
implies that `x0 = 1, X = 3, y0 = 1, N = 10`

Once set, press ENTER. You will see `Working...` in your teminal. This stage usually doesn't take too long, but some input conditions might prolong computation time. Please wait until it is finished.

Two windows with plots should appear (the second one may appear behind the first one):
![](https://i.imgur.com/z9Wyds9.png)

![](https://i.imgur.com/IBcHNdL.png)

Let's look at both of them
1. The first window contains 4 plots. First three are for the three methods of approximation compared with exact IVP solution. The last plot shows local error (vertical axis) for each of the methods, on specified x (horizontal axis). Different methods are indicated by different colors, which are specified in legend.
2. The second windows has just one plot, which shows the maximum error (vertical axis) of each of the methods when one launched on specified number of grid cells (horizontal axis). Likewise, methods are indicated by different colors. The red vertical dashed line indicates the number of grid cells that was specified initially. 

At the left bottom there are control buttons, which can be used for zooming/panning. 
You can see the exact position of your cursor at the bottom right. 

In order to shut down the program, close all GUI windows with plots.

### 4. Numerical Investigation
Number of grid cells = 10
![](https://i.imgur.com/iLHfl0q.png)
![](https://i.imgur.com/jwWtuKz.png)
![](https://i.imgur.com/9gqcdry.png)
![](https://i.imgur.com/Uo3RwWB.png)
![](https://i.imgur.com/3W5CuSo.png)
(Some anomaly in Improved Euler method at (2.5, 4), perhaps because of geometry of the f(x))

Zoomed Global Errors plot:
![](https://i.imgur.com/1O5voC3.png)

All of the approximation methods' global errors approach 0. Euler method does that significantly slower than the other two.
