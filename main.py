import matplotlib.pyplot as plt
import numpy as np
import math

# Initial conditions
x0 = 1
X = 3
y0 = 1
N = 10
precision = (X - x0 + 1) / N

f = lambda x, y: (math.sin(x)) ** 2 + y * (math.cos(x) / math.sin(x))  # Given f(x)
exact_solution = np.vectorize(
    lambda x: math.sin(x) * (y0 / math.sin(x0) + math.cos(x0) - math.cos(x)))  # IVP "Solution"


# Defining methods of approximation (for one step basing on previous step)
def euler(prev_step, prev_y, step_size):
    return f(prev_step, prev_y) * step_size + prev_y


def euler_improved(prev_step, prev_y, step_size):
    k1 = step_size * f(prev_step, prev_y)
    k2 = step_size * f(prev_step + step_size, prev_y + k1)
    return prev_y + (k1 + k2) / 2


def runge_kutta(prev_step, prev_y, step_size):
    k1 = step_size * f(prev_step, prev_y)
    k2 = step_size * f(prev_step + step_size / 2, prev_y + k1 / 2)
    k3 = step_size * f(prev_step + step_size / 2, prev_y + k2 / 2)
    k4 = step_size * f(prev_step + step_size, prev_y + k3)
    return prev_y + (k1 + 2 * k2 + 2 * k3 + k4) / 6


def approximate(method, steps, step_size):
    """
    Generate an array of values, calculated on given array of steps using specified method
    :param method: specified method
    :param steps: array of steps
    :param step_size: size of step
    :return:
    """
    number_of_steps = len(steps)

    y = np.zeros(number_of_steps)
    err = np.zeros(number_of_steps)

    y[0] = y0
    max_err = -1
    aver_err = 0

    for i in range(1, number_of_steps):
        y[i] = method(steps[i - 1], y[i - 1], step_size)
        ex_sol = exact_solution([steps[i]])[0]
        err[i] = abs(ex_sol - y[i])
        aver_err += err[i]
        if err[i] > max_err:
            max_err = err[i]
    return y, err, max_err, aver_err / len(err)


def main():
    global x0, X, y0, N, precision
    print("Input the initial conditions x0, X, y0, N")
    print("(x0 - start x point, X - end x point, y0 - initial value, N - grid blocks per unit)")
    print("Default input: %d %d %d %d" % (x0, X, y0, N))
    print("(press ENTER to use defaults)")
    inp = input()
    if inp:
        inp = inp.split()
        x0 = float(inp[0])
        X = float(inp[1])
        y0 = float(inp[2])
        N = int(inp[3])
        precision = 1 / N
    else:
        print("Using default conditions.")

    print("Working...")

    # ---------- Approximations and Local errors ----------

    # Enumerating values of x for drawing the plot
    xs = np.arange(x0, X + precision, precision)
    if xs[-1] > X:  # Don't go beyond the given X
        xs = xs[:-1]

    # Generating values using given xs and precision
    eu = approximate(euler, xs, precision)
    eu_en = approximate(euler_improved, xs, precision)
    run_kut = approximate(runge_kutta, xs, precision)

    # Generating values of perfect plot
    perfect_plot = exact_solution(xs)

    # ---------- Global errors ----------

    euler_max_errors = []
    euler_improved_max_errors = []
    runge_max_errors = []

    # Enumerating from 2 to N*2 (number of grid cells) for plot drawing
    breakdown = [x for x in range(2, N * 2)]
    # Enumerating step sizes by dividing the range by x from range (2, x*2)
    step_sizes = [(X - x0 + 1) / x for x in range(2, N * 2)]
    for step_size in step_sizes:
        # Calculate the max errors
        tmp_xs = np.arange(x0, X + step_size, step_size)
        if tmp_xs[-1] > X:  # Don't go beyond the given X
            tmp_xs = tmp_xs[:-1]
        euler_max_errors.append(approximate(euler, tmp_xs, step_size)[2])
        euler_improved_max_errors.append(approximate(euler_improved, tmp_xs, step_size)[2])
        runge_max_errors.append(approximate(runge_kutta, tmp_xs, step_size)[2])

    print("Calculations done. Close the GUI windows in order to finish the program.")

    # ---------- Drawing plots ----------

    # Figure 1 (Approximation vs Perfect plot & Local errors plot)
    fig = plt.figure(1, figsize=(15, 15))
    fig.canvas.set_window_title('IVP Solution Approximation | Step size = %f' % precision)

    sub = plt.subplot(221)
    sub.set_title("Euler method")
    sub.plot(xs, perfect_plot, label="Perfect", color='green')
    sub.plot(xs, eu[0], label="Approximation", color='red')
    plt.legend()

    sub = plt.subplot(222)
    sub.set_title("Improved Euler method")
    sub.plot(xs, perfect_plot, label="Perfect", color='green')
    sub.plot(xs, eu_en[0], label="Approximation", color='red')
    plt.legend()

    sub = plt.subplot(223)
    sub.set_title("Runge-Kutta method")
    sub.plot(xs, perfect_plot, label="Perfect", color='green')
    sub.plot(xs, run_kut[0], label="Approximation", color='red')
    plt.legend()

    sub = plt.subplot(224)
    sub.set_title("Local Errors")
    sub.plot(xs, eu[1], label="Euler")
    sub.plot(xs, eu_en[1], label="Improved Euler")
    sub.plot(xs, run_kut[1], label="Runge Kutta")
    plt.xlabel('X')
    plt.ylabel('Local error')
    plt.legend()

    # Figure 2 (Max errors by step size)
    fig2 = plt.figure(2, figsize=(10, 5))
    fig2.canvas.set_window_title('Max error depending on number of grid cells')

    plt.title("Max error depending on number of grid cells")
    plt.plot(breakdown, euler_max_errors, label="Euler")
    plt.plot(breakdown, euler_improved_max_errors, label="Improved Euler")
    plt.plot(breakdown, runge_max_errors, label="Runge Kutta")
    plt.xlabel('Number of grid cells')
    plt.ylabel('Max error')
    plt.axvline(N, color='red', alpha=0.5, linestyle='dashed', label="Current number of grid cells")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
